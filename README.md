# Awesome Distributed Systems

[Pareto](https://en.wikipedia.org/wiki/Pareto_principle) reading list on distributed systems

## Categories

⭐️ – influential papers / talks,
💎 – hidden gems,
👩‍🎓 – study materials,
🍿 – fun,
📐 – TLA+ specifications,
📕 – PhD or book, 
📚 – reading list,
🚧 – under construction

---

## Tune in

- [Scenes from Distributed Systems](https://drawings.jvns.ca/drawings/distributed-systems.svg) by [Julia Evans](https://jvns.ca/)
- [distributed-system.txt](https://lamport.azurewebsites.net/pubs/distributed-system.txt) by [Leslie Lamport](https://lamport.org/)
- [There are only two hard problems in distributed systems...](https://twitter.com/mathiasverraes/status/632260618599403520)

---

- [Why do we need distributed systems?](https://brooker.co.za/blog/2020/01/02/why-distributed.html) by [Mark Brooker](https://brooker.co.za/blog/)


## Reading Lists 📚

- [Distributed Consensus Reading List](https://github.com/heidihoward/distributed-consensus-reading-list) by Heidi Howard
- [Testing Distributed Systems](https://asatarin.github.io/testing-distributed-systems/) by [Andrey Satarin](https://asatarin.github.io/)
- [Paxosmon: Gotta Consensus Them All](https://vadosware.io/post/paxosmon-gotta-concensus-them-all/)
- [Resilience engineering papers](https://github.com/lorin/resilience-engineering) by [Lorin Hochstein](https://lorinhochstein.org/)
- [My Writings](http://lamport.azurewebsites.net/pubs/pubs.html) by Leslie Lamport

## Consistency 

- 🔥 [Consistency Models](https://jepsen.io/consistency) by [Kyle Kingsbury](https://aphyr.com/about)
- ⭐️ [Linearizability: A Correctness Condition for Concurrent Objects](https://cs.brown.edu/~mph/HerlihyW90/p463-herlihy.pdf) by Maurice Herlihy and Jannette Wing, 1990 
- 💎 [Reading the Herlihy & Wing Linearizability paper with TLA+](https://github.com/lorin/tla-linearizability), 📐 [TLA+ Spec](https://github.com/lorin/tla-linearizability/blob/master/Linearizability.tla) by Lorin Hochstein & [Markus Alexander Kuppe](https://github.com/lemmy)
- [Please stop calling databases CP or AP](https://martin.kleppmann.com/2015/05/11/please-stop-calling-databases-cp-or-ap.html) by [Martin Kleppmann](https://martin.kleppmann.com/)

## Consensus

- 📚 [Distributed Consensus Reading List](https://github.com/heidihoward/distributed-consensus-reading-list) by Heidi Howard

### Protocols

#### Paxos

_The Paxos algorithm, when presented in plain English, is very simple_ – Leslie Lamport, _Paxos Made Simple_

- ⭐️🍿 [Part-Time Parliament](https://lamport.azurewebsites.net/pubs/lamport-paxos.pdf), by Leslie Lamport, 1990 → 1998
- ⭐️ [Paxos Made Simple](https://lamport.azurewebsites.net/pubs/paxos-simple.pdf) by Leslie Lamport, 2001
- 🍿 History behind [Part-Time Parliament](http://lamport.azurewebsites.net/pubs/pubs.html#lamport-paxos) and [Paxos Made Simple](http://lamport.azurewebsites.net/pubs/pubs.html#paxos-simple)
- 👩‍🎓 [Paxos Lecture](http://youtu.be/JEpsBg0AO6o), [Slides](https://ongardie.net/static/raft/userstudy/paxos.pdf) by John Ousterhout and Diego Ongaro
- 👩‍🎓 Single-Decree Paxos [pseudocode](https://pdos.csail.mit.edu/archive/6.824-2013/notes/paxos-code.html) by [MIT 6.824](https://pdos.csail.mit.edu/6.824/), 📐 [TLA+ Spec](https://github.com/tlaplus/Examples/blob/master/specifications/Paxos/Paxos.tla) by Leslie Lamport
- 🍿 [Who are the legislators of Paxos](https://cs.stackexchange.com/questions/12401/who-are-the-legislators-of-paxos)
- 📚 [Paxosmon: Gotta Consensus Them All](https://vadosware.io/post/paxosmon-gotta-concensus-them-all/) 

##### Paxos Gems

- 💎 [Virtual Consensus in Delos](https://www.usenix.org/conference/osdi20/presentation/balakrishnan) by Mahesh Balakrishnan et al.
- 💎 [CASPaxos: Replicated State Machines Without Logs](https://arxiv.org/abs/1802.07000) by [Denis Rystsov](http://rystsov.info/)
- 💎 [Flexible Paxos: Quorum Intersection Revisited](https://arxiv.org/pdf/1608.06696.pdf) by Heidi Howard

##### Paxos in Production

- [Paxos Made Live - An Engineering Perspective](https://www.cs.utexas.edu/users/lorenzo/corsi/cs380d/papers/paper2-1.pdf)
- [Millions of Tiny Databases](https://www.usenix.org/system/files/nsdi20-paper-brooker.pdf)
- [Virtual Consensus in Delos](https://www.usenix.org/system/files/osdi20-balakrishnan.pdf)
- [Paxos Quorum Leases: Fast Reads Without Sacrificing Writes](http://www.cs.cmu.edu/~imoraru/papers/qrl.pdf)

#### Raft

_We were thinking about the island of Paxos and how to escape it_ – Diego Ongaro

- ⭐️ [In Search of an Understandable Consensus Algorithm](https://raft.github.io/raft.pdf) by John Ousterhout and Diego Ongaro, 2014
- [Raft Consensus Algorithm](https://raft.github.io/)
- 📕 [Consensus: Bridging Theory and Practice](https://github.com/ongardie/dissertation) – PhD thesis by Diego Ongaro
- 👩‍🎓 [Raft Lecture](https://www.youtube.com/watch?v=YbZ3zDzDnrw), [Slides](https://raft.github.io/slides/raftuserstudy2013.pdf)
- 📐 [TLA+ Spec](https://github.com/ongardie/raft.tla/blob/master/raft.tla)
- [State Machine](https://github.com/etcd-io/raft/blob/main/raft.go) from https://github.com/etcd-io/raft
- 👩‍🎓 [Implementing Raft](https://eli.thegreenplace.net/2020/implementing-raft-part-0-introduction/)
- [Raft does not Guarantee Liveness in the face of Network Faults](https://decentralizedthoughts.github.io/2020-12-12-raft-liveness-full-omission/)
- 🍿 [Why the "Raft" name?](https://groups.google.com/forum/#!topic/raft-dev/95rZqptGpmU)

#### Less Famous: VR, ZAB

- [Viewstamped Replication Revisited](https://pmg.csail.mit.edu/papers/vr-revisited.pdf), [originally](https://pmg.csail.mit.edu/papers/vr.pdf) by Brian Oki & Barbara Liskov, 1988
- [Zab: High-performance broadcast for primary-backup systems](https://marcoserafini.github.io/papers/zab.pdf) by Junqueira, Reed, Serafini, 2011

### Math

- ⭐️ [Impossibility of Distributed Consensus with One Faulty Process](https://groups.csail.mit.edu/tds/papers/Lynch/jacm85.pdf) – FLP theorem – by Fischer, Lynch, Patterson, 1985

#### Failure Detectors

- ⭐️ [Unreliable Failure Detectors for Reliable Distributed Systems](https://citeseerx.ist.psu.edu/doc/10.1.1.113.498) by Tushar Deepak Chandra and Sam Toueg, 1996
- [The Weakest Failure Detector for Solving Consensus](https://citeseerx.ist.psu.edu/pdf/1eb6ffee1f322412d9d76190fc76b3dcc6546cee) by Chandra & Toueg

#### Quorum Systems

- [The Origin of Quorum Systems](https://vukolic.com/QuorumsOrigin.pdf) by Marko Vukolic
- [The Load, Capacity and Availability of Quorum Systems](https://www.wisdom.weizmann.ac.il/~naor/PAPERS/quor.pdf)
- [Byzantine Quorum Systems](https://dahliamalkhi.files.wordpress.com/2015/12/byzquorums-distcomputing1998.pdf) by Dahlia Malkhi & Michael Reiter

## Transactions

### Isolation

#### Levels

- [Introduction to Transaction Isolation Levels](http://dbmsmusings.blogspot.com/2019/05/introduction-to-transaction-isolation.html), [Correctness Anomalies Under Serializable Isolation](https://dbmsmusings.blogspot.com/2019/06/correctness-anomalies-under.html) by Daniel Abadi
- [Elle: Inferring Isolation Anomalies from Experimental Observations](https://people.ucsc.edu/~palvaro/elle_vldb21.pdf) by Kyle Kingsbury & Peter Alvaro


#### Protocols

##### Snapshot Isolation (SI)

- 👩‍🎓 📐 [TLA+ Spec](https://github.com/will62794/snapshot-isolation-spec/blob/master/SnapshotIsolation.tla)
- [A Read-Only Transaction Anomaly Under Snapshot Isolation](https://www.cs.umb.edu/~poneil/ROAnom.pdf)

##### Serializable Snapshot Isolation (SSI)

- [Making Snapshot Isolation Serializable](https://dsf.berkeley.edu/cs286/papers/ssi-tods2005.pdf) by Alan Fekete et al.
- 📕 [Serializable Isolation for Snapshot Databases](https://ses.library.usyd.edu.au/bitstream/2123/5353/1/michael-cahill-2009-thesis.pdf) – PhD thesis by Michael Cahill
- [Serializable Snapshot Isolation in PostgreSQL](https://drkp.net/papers/ssi-vldb12.pdf)
- 📐 [TLA+ Spec](https://github.com/pron/amazon-snapshot-spec/blob/master/serializableSnapshotIsolation.tla) by Ron Pressler

### Distributed Transactions

- [Consensus on Transaction Commit](https://www.microsoft.com/en-us/research/uploads/prod/2004/01/consensus-on-transaction-commit.pdf) by Jim Gray & Leslie Lamport 
- [Comparing Distributed Transaction Architectures for the Cloud Era](https://www.youtube.com/watch?v=w_zYYF3-iSo) by Kyle Kingsbury

#### Percolator (SI + 2PC)

- [Large-scale Incremental Processing Using Distributed Transactions and Notifications](https://static.googleusercontent.com/media/research.google.com/en//pubs/archive/36726.pdf)
- 📐 [TLA+ Spec](https://github.com/pingcap/tla-plus/blob/master/Percolator/Percolator.tla)

#### Spanner (2PL + 2PC + MVCC)

- ⭐️ [Spanner: Google’s Globally Distributed Database](https://dl.acm.org/doi/pdf/10.1145/2491245), 2012
- [Talk + Slides](https://www.usenix.org/conference/osdi12/technical-sessions/presentation/corbett)

#### Deterministic Transactions 

##### Calvin

- [It’s Time to Move on from Two Phase Commit](http://dbmsmusings.blogspot.com/2019/01/its-time-to-move-on-from-two-phase.html) by Daniel Abadi
- ⭐️ [Calvin: Fast Distributed Transactions for Partitioned Database Systems](http://cs.yale.edu/homes/thomson/publications/calvin-sigmod12.pdf) by Abadi et al., 2012
- [The Case for Determinism in Database Systems](https://www.cs.umd.edu/~abadi/papers/determinism-vldb10.pdf)
- [An Overview of Deterministic Database Systems](https://dl.acm.org/doi/pdf/10.1145/3181853)

##### YDB

- [Распределенные транзакции в YDB](https://www.highload.ru/moscow/2019/abstracts/5324)
- [YDB: мультиверсионность в распределенной базе](https://highload.ru/foundation/2022/abstracts/8317)

## Scheduling

- ⭐️ [Large-scale cluster management at Google with Borg](https://static.googleusercontent.com/media/research.google.com/ru//pubs/archive/43438.pdf)
- ⭐️ [Quincy: Fair Scheduling for Distributed Computing Clusters](https://www.sigops.org/s/conferences/sosp/2009/papers/isard-sosp09.pdf)
- ⭐️ [Dominant Resource Fairness: Fair Allocation of Multiple Resource Types](https://cs.stanford.edu/~matei/papers/2011/nsdi_drf.pdf)
- [Firmament: fast, centralized cluster scheduling at scale](https://pdos.csail.mit.edu/papers/firmament:osdi16.pdf)
- [Sparrow: Distributed, Low Latency Scheduling](https://cs.stanford.edu/~matei/papers/2013/sosp_sparrow.pdf)

## Storage

### Drives

- 👩‍🎓 [Hard Disk Drives](https://pages.cs.wisc.edu/~remzi/OSTEP/file-disks.pdf), [Flash-based SSDs](https://pages.cs.wisc.edu/~remzi/OSTEP/file-ssd.pdf)
- [Discovering Hard Disk Physical Geometry through Microbenchmarking](http://blog.stuffedcow.net/2019/09/hard-disk-geometry-microbenchmarking/)
- 🍿 [Shouting in the Datacenter](https://www.youtube.com/watch?v=tDacjrSCeq4) by Brendan Gregg & Bryan Cantrill
- [What every programmer should know about solid-state drives](http://codecapsule.com/2014/02/12/coding-for-ssds-part-6-a-summary-what-every-programmer-should-know-about-solid-state-drives/)

### LSM 🚧

- [LevelDB](https://github.com/google/leveldb), [RocksDB](https://github.com/facebook/rocksdb)
- [RocksDB: Evolution of Development Priorities in a Key-value Store Serving Large-scale Applications](https://www.google.com/url?q=https://dl.acm.org/doi/10.1145/3483840&sa=D&source=docs&ust=1638550846630000&usg=AOvVaw3gg_7Hl7RUWGxkQEAbJpNk)

### Crash Consistency

- [All File Systems Are Not Created Equal: On the Complexity of Crafting Crash-Consistent Applications](https://www.usenix.org/conference/osdi14/technical-sessions/presentation/pillai)
- [Can Applications Recover from fsync Failures?](https://www.usenix.org/conference/atc20/presentation/rebello), [PostgreSQL vs. fsync](https://archive.fosdem.org/2019/schedule/event/postgresql_fsync/)
- [Protocol-Aware Recovery for Consensus-Based Storage](https://www.usenix.org/conference/fast18/presentation/alagappan)

### Erasure Codes 🚧

- ⭐️ [Erasure Coding in Windows Azure Storage](https://www.usenix.org/conference/atc12/technical-sessions/presentation/huang)

### Hardware Errors @ Scale

- [DRAM Errors in the Wild: A Large-Scale Field Study](https://www.cs.toronto.edu/~bianca/papers/sigmetrics09.pdf)
- [Silent Data Corruptions at Scale](https://arxiv.org/pdf/2102.11245.pdf)
- [Cores that don’t count](https://research.google/pubs/pub50337/)

## Testing & Verification

- 📚 [Testing Distributed Systems](https://asatarin.github.io/testing-distributed-systems/) by Andrey Satarin

### TLA+ 

_Weeks of debugging can save you hours of TLA+_

- [Use of Formal Methods at Amazon Web Services](https://lamport.azurewebsites.net/tla/formal-methods-amazon.pdf)
- [Code Only Says What it Does](https://brooker.co.za/blog/2020/06/23/code.html) by Mark Brooker

#### Entry

- https://github.com/tlaplus
- [The TLA+ Home Page](https://lamport.azurewebsites.net/tla/tla.html)


#### Study 👩‍🎓

- [The TLA+ Video Course](https://lamport.azurewebsites.net/video/videos.html) by Leslie Lamport
- [Learn TLA+](https://learntla.com/index.html) by Hillel Wayne
- [Examples](https://github.com/tlaplus/Examples)
- 💎 [Blocking Queue](https://github.com/lemmy/BlockingQueue) by Markus Alexander Kuppe

#### Deep Dive

- [The Temporal Logic of Actions](https://lamport.azurewebsites.net/pubs/lamport-actions.pdf) by Leslie Lamport
- 📕 [Specifying Systems](https://lamport.azurewebsites.net/tla/book.html) by Leslie Lamport
- 💎 [What Good is Temporal Logic?](https://lamport.azurewebsites.net/pubs/what-good.pdf) by Leslie Lamport
- 💎 [TLA+ In Practice and Theory](https://pron.github.io/posts/tlaplus_part1) by Ron Pressler

### Deterministic Simulation

- ⭐️ [Testing Distributed Systems w/ Deterministic Simulation](https://www.youtube.com/watch?v=4fFDFbi3toc) by Will Wilson
- [FoundationDB or: How I Learned to Stop Worrying and Trust the Database](https://www.youtube.com/watch?v=OJb8A6h9jQQ)

### Fault Injection

- https://jepsen.io/
- https://github.com/jepsen-io/jepsen
- [Talks](https://jepsen.io/talks) by Kyle Kingsbury

### Bugs

- ⭐️ [Jepsen / Analyses](https://jepsen.io/analyses)
- [All File Systems Are Not Created Equal: On the Complexity of Crafting Crash-Consistent Applications](https://www.usenix.org/conference/osdi14/technical-sessions/presentation/pillai)
- [Redundancy Does Not Imply Fault Tolerance: Analysis of Distributed Storage Reactions to Single Errors and Corruptions](https://www.usenix.org/conference/fast17/technical-sessions/presentation/ganesan)

## Byzantine Fault Tolerance

### Protocols

#### PBFT

- ⭐️ [Practical Byzantine Fault Tolerance](https://pmg.csail.mit.edu/papers/osdi99.pdf) – paper by Miguel  Castro & Barbara Liskov, 1999
- 📕 [Practical Byzantine Fault Tolerance](https://www.microsoft.com/en-us/research/wp-content/uploads/2017/01/thesis-mcastro.pdf) – PhD thesis by Miguel Castro
- 👩‍🎓 [Talk](https://www.youtube.com/watch?v=Q0xYCN-rvUs) by Castro, 👩‍🎓 [Talk](https://www.youtube.com/watch?v=Uj638eFIWg8) by Liskov
- 👩‍🎓 [From Viewstamped Replication to Byzantine Fault Tolerance](https://pmg.csail.mit.edu/papers/vr-to-bft.pdf)

#### Bitcoin

- ⭐️ [Bitcoin: A Peer-to-Peer Electronic Cash System](https://www.ussc.gov/sites/default/files/pdf/training/annual-national-training-seminar/2018/Emerging_Tech_Bitcoin_Crypto.pdf) by Satoshi Nakamoto, 2008
- ⭐️ [Majority is not Enough: Bitcoin Mining is Vulnerable](https://www.cs.cornell.edu/~ie53/publications/btcProcFC.pdf)
- 👩‍🎓 [Bitcoin and Cryptocurrency Technologies](https://bitcoinbook.cs.princeton.edu/)
- https://github.com/bitcoin/bitcoin

#### HotStuff 

- ⭐️ [HotStuff: BFT Consensus with Linearity and Responsiveness](https://research.vmware.com/files/attachments/0/0/0/0/0/7/7/podc.pdf)

### Impossibility

- [Byzantine Generals with no PKI (FLM)](https://decentralizedthoughts.github.io/2019-08-02-byzantine-agreement-is-impossible-for-$n-slash-leq-3-f$-is-the-adversary-can-easily-simulate/)
- [Byzantine Generals in Partial Synchrony](https://decentralizedthoughts.github.io/2019-06-25-on-the-impossibility-of-byzantine-agreement-for-n-equals-3f-in-partial-synchrony/)

### Cryptography

- [What is a Cryptographic Hash Function?](https://decentralizedthoughts.github.io/2020-08-28-what-is-a-cryptographic-hash-function/)
- [Hash Functions, Random Oracles, and Bitcoin](https://intensecrypto.org/public/lec_07_hash_functions.html)
- [What is a Merkle Tree?](https://decentralizedthoughts.github.io/2020-12-22-what-is-a-merkle-tree/)
- [Diffie–Hellman key exchange](https://en.wikipedia.org/wiki/Diffie%E2%80%93Hellman_key_exchange)
- [Elliptic Curve Cryptography: a gentle introduction](https://andrea.corbellini.name/2015/05/17/elliptic-curve-cryptography-a-gentle-introduction/)
- [What are zk-SNARKs?](https://z.cash/technology/zksnarks/)

## Systems 🚧

### Foundation ⭐️

- [The Google File System](https://research.google/pubs/pub51/) by Google, 2003
- [MapReduce: Simplified Data Processing on Large Clusters](https://research.google/pubs/pub62/) by Google, 2004
- [The Chubby lock service for loosely-coupled distributed systems](https://research.google/pubs/pub27897/) by Google, 2006 
- [Bigtable: A Distributed Storage System for Structured Data](https://research.google/pubs/pub27898/) by Google, 2006 
- [Dynamo: Amazon’s Highly Available Key-value Store](https://www.allthingsdistributed.com/files/amazon-dynamo-sosp2007.pdf) by Amazon, 2007 
- [Spanner: Google's Globally-Distributed Database](https://research.google/pubs/pub39966/) by Google, 2012

### Misc

- ⭐️ [The Tail at Scale](https://www.barroso.org/publications/TheTailAtScale.pdf) by Jeff Dean & Luiz André Barroso
- ⭐️ [Exponential Backoff And Jitter](https://aws.amazon.com/blogs/architecture/exponential-backoff-and-jitter/) by Mark Brooker
- ⭐️ [How NOT to Measure Latency](https://www.youtube.com/watch?v=lJ8ydIuPFeU) by Gil Tene
- 📚 [Resilience engineering papers](https://github.com/lorin/resilience-engineering) by Lorin Hochstein
- [Addressing Cascading Failures](https://sre.google/sre-book/adressing-cascading-failures/)
- [Queueing Theory in Practice: Performance Modeling for the Working Engineer](https://www.youtube.com/watch?v=Hda5tMrLJqc)
- [What's the Cost of a Millisecond?](https://www.youtube.com/watch?v=JgrcaK0WQCQ)
- [Jurassic Cloud](https://www.usenix.org/publications/loginonline/jurassic-cloud)
- [Systems Performance: Enterprise and the Cloud](https://www.brendangregg.com/systems-performance-2nd-edition-book.html) by Brendan Gregg

#### Metastable Failures

- [Metastable Failures in Distributed Systems](https://sigops.org/s/conferences/hotos/2021/papers/hotos21-s11-bronson.pdf)
- [Metastable Failures in the Wild](https://www.usenix.org/conference/osdi22/presentation/huang-lexiang)
- [Solving the Mystery of Link Imbalance: A Metastable Failure State at Scale](https://engineering.fb.com/2014/11/14/production-engineering/solving-the-mystery-of-link-imbalance-a-metastable-failure-state-at-scale/)
- [Caches, Modes, and Unstable Systems](https://brooker.co.za/blog/2021/08/27/caches.html)
